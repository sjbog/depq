// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

package depq

import	(
	"fmt"
//	"bitbucket.org/sjbog/depq"
)


type My_Item_Ref	struct {
	Value	int
}

func ( self  * My_Item_Ref )	Is_Less ( other  Item_Interface )	( bool )	{

	return	self.Value < other.( * My_Item_Ref ).Value
}

func ( self  * My_Item_Ref )	Is_More ( other  Item_Interface )	( bool )	{

	return	self.Value > other.( * My_Item_Ref ).Value
}

func ( self  * My_Item_Ref )	Copy ()	( Item_Interface )	{

	return	& My_Item_Ref { ( * self ).Value }
}


func ExamplePQ_itemsByReference ()	{

	var pq	= New_PQ ()

	pq.Push ( & My_Item_Ref { 4 } )
	pq.Push ( & My_Item_Ref { 2 } )
	pq.Push ( & My_Item_Ref { 1 } )


	var pq2	= New_PQ ()
	pq2.Push ( & My_Item_Ref { 3 } )
	pq2.Push ( & My_Item_Ref { -4 } )

//	Shallow copy of items ( by reference )
	pq.Merge ( pq2 )


	for	pq.Len () > 0	{
		fmt.Println ( pq.Pop () .( * My_Item_Ref ).Value )
	}

//	Output:
//-4
//1
//2
//3
//4
}