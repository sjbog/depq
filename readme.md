Package Implements DEPQ ( [Double Ended Priority Queue]( https://en.wikipedia.org/wiki/Double-ended_priority_queue ) ) with [Golang ( google go )]( https://golang.org/ )
============
[![GoDoc](https://godoc.org/bitbucket.org/sjbog/depq?status.svg)](https://godoc.org/bitbucket.org/sjbog/depq)
[![Go Walker](http://gowalker.org/api/v1/badge)](https://gowalker.org/bitbucket.org/sjbog/depq)

Priority Queue uses struct comparison methods instead of package ones.
Thus gives the ability to build custom Priority Queue, while "container/heap" is array ( slice ) based only.

PQ and DEPQ are array ( slice ) based structures, which implement  the corresponding interfaces. One can build tree based PQ with the ability to merge any struct which implements the interface.

Docs:
[ GoWalker ]( https://gowalker.org/bitbucket.org/sjbog/depq )
or [ GoDoc ]( https://godoc.org/bitbucket.org/sjbog/depq )


### To install / update
version control : [git]( https://en.wikipedia.org/wiki/Git_%28software%29 ) ( ```git --version``` )

```go get -u -v bitbucket.org/sjbog/depq```

### To test
100% test coverage

```
go test -v -parallel=4 -cover bitbucket.org/sjbog/depq
> ok    bitbucket.org/sjbog/depq    0.028s  coverage: 100.0% of statements
```

####Benchmarks

```go test -v -parallel=4 -bench . bitbucket.org/sjbog/depq```

####Code coverage html view ( will write a coverage_profile file )

```
go test -parallel=4 -coverprofile="coverage_profile.out" bitbucket.org/sjbog/depq
go tool cover -html="coverage_profile.out"
```


### License : BSD 3-Clause, see LICENSE file

##
Tags: golang depq implementation, go depq, golang double ended priority queue