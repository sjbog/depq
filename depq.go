// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

/*
Double Ended Priority Queue	( https://en.wikipedia.org/wiki/Double-ended_priority_queue )
*/
package depq

/*
Array ( slice ) based implementation of double ended priority queue.	Implements DEPQ_Interface.

Example usage:

1) Create a struct which implements Item_Interface

	type My_Item struct {
		Value	int
	}

	func ( self  My_Item )	Is_Less ( other  Item_Interface )	bool	{
		return	self.Value < other.( My_Item ).Value
	}

	// implement Is_More and Copy

2) Create a new ( array based ) PQ
	var pq	= depq.New_DEPQ ( My_Item { 15 }, My_Item { 11 }, My_Item { 2 }, My_Item { 4 }, My_Item { 8 } )

	pq.Peek ()	// == 2	<= shows Min value, but doesn't pop
	pq.Peek_Max ()	// == 15	<= shows Max value, but doesn't pop

	pq.Push ( My_Item { -1 } )

	pq.Pop ()	// == -1	<= pops Min value
	pq.Pop_Max ()	// == 15	<= pops Max value

*/
type DEPQ [] Item_Interface

/*	Abstract unit ( parent class in OOP ), which DEPQ produce and consume. Can be downgraded to PQ_Interface
*/
type DEPQ_Interface interface {

	PQ_Interface

	Pop_max		()	Item_Interface
	Peek_max	()	Item_Interface
}

/*	Creates and fills a new Double Ended Priority Queue
*/
func New_DEPQ ( items  ... Item_Interface )		* DEPQ	{

	var	(
		size	= len ( items )
		depq	= make ( DEPQ, 0, size +8 )
	)

	for	i := 0 ; i < size ; i ++	{
			depq.Push ( items [ i ] )
	}

	return	& depq
}

/*	In-place restructure. Is used to sort raw slice into a Double Ended Priority Queue ordered slice
*/
func ( self  * DEPQ )	Heapify ( offset  uint )	{

	for	i, size := offset, self.Len () ; i < size ; i ++	{
			self.propagate_up ( i )
	}
}

/*	Makes a deep copy of current values. Calls Item_Interface.Copy () on every item
*/
func ( self  * DEPQ )	Copy ()	( PQ_Interface )	{
	var	(
		size	= self.Len ()
		pq	= make ( DEPQ, size, cap ( * self ) )
	)

	for	i := uint ( 0 ); i < size ; i ++	{
		pq [ i ]	= ( * self )[ i ].Copy ()
	}

	return	& pq
}

/*	Pushes values of `other` PQ into self. Copy by reference on pointer values ( shallow copy )

		Warning : this operation will purge `other` ( it will be empty ).

	To make a merge with deep copy use
		pq1.Merge ( pq2.Copy () )
*/
func ( self  * DEPQ )	Merge ( other  PQ_Interface )	{

	if	other == nil	{	return	}

	var size	= other.Len ()

	if	size == 0	{	return	}

//	Fast copy for 2 slice based DEPQs
	if	other_depq, ok	:= other.( * DEPQ ); ok	&& self.Len () == 0	{

		* self	= * other_depq
		self.Heapify ( 1 )
		return

	}	else	{
//	Shallow copy by PQ_Interface
		for	i := uint ( 0 ) ; i < size ; i++	{
			self.Push ( other.Pop () )
		}
	}
}

/*	Returns number of items in the underlying array ( length of slice, not capacity )
*/
func ( self  * DEPQ )	Len ()		uint	{	return	uint ( len ( * self ) )	}

/*	Appends item to the slice and propagates it up
*/
func ( self  * DEPQ )	Push ( item  Item_Interface )	{

	( * self )	= append ( ( * self ), item.( Item_Interface ) )
	self.propagate_up ( self.Len () -1 )
}


/*	Propagates the item to the top
*/
func ( self  * DEPQ )	propagate_up ( index  uint )	{

	var	(
		parent_index	uint
		left_node	= false
	)

	if	index == 0	{	return	}

//	Determine order in last cell
	if	index % 2 == 1	{

//		Left node
		if	( * self ) [ index ].Is_Less ( ( * self ) [ index -1 ] )	{

			self.swap ( index, index -1 )
			index --
			left_node	= true
		}

	} else {

//		Determine should it go left or right in parent

//		Don't simplify : rounding after "/4" is required
//		Parent left node ( low value )
		parent_index	= ( ( index -2 ) / 4 ) * 2

		if	( * self ) [ index ].Is_Less ( ( * self ) [ parent_index ] )	{

			self.swap ( index, parent_index )
			index	= parent_index
			left_node	= true

		} else if	( * self ) [ index ].Is_More ( ( * self ) [ parent_index +1 ] )	{

			parent_index ++

			self.swap ( index, parent_index )
			index	= parent_index
			left_node	= false

		} else {
			return
		}
	}


	if	left_node	{

		parent_index	= ( ( index -2 ) / 4 ) * 2

		for	index > 0	&&
			( * self ) [ index ].Is_Less ( ( * self ) [ parent_index ] )	{

			self.swap ( index, parent_index )
			index	= parent_index
			parent_index	= ( ( index -2 ) / 4 ) * 2
		}

	} else {

		parent_index	= ( ( index -2 ) / 4 ) * 2 + 1

		for	index > 1	&&
			( * self ) [ index ].Is_More ( ( * self ) [ parent_index ] )	{

			self.swap ( index, parent_index )
			index	= parent_index
			parent_index	= ( ( index -2 ) / 4 ) * 2 + 1
		}
	}
}

/*	Swaps slice items by indices
*/
func ( self  * DEPQ )	swap ( index1, index2  uint )	{

	( * self )[ index1 ], ( * self )[ index2 ]	= ( * self )[ index2 ], ( * self )[ index1 ]
}

/*	Shows top Min item but doesn't pop it
*/
func ( self  * DEPQ )	Peek ()		Item_Interface	{

	if	self.Len () == 0	{
		return	nil
	}
	return	( * self ) [ 0 ]
}

/*	Shows top Max item but doesn't pop it
*/
func ( self  * DEPQ )	Peek_max ()		Item_Interface	{

	if	self.Len () > 1	{
		return	( * self ) [ 1 ]
	}
	if	self.Len () == 0	{
		return	nil
	}
	return	( * self ) [ 0 ]
}

/*	Shows and Removes top Min item from the Priority Queue
*/
func ( self  * DEPQ )	Pop ()		( item  Item_Interface )	{

	var size	= self.Len ()

	switch	size	{

		case	0 :	return	nil

		case	1 :

			item	= ( * self ) [ 0 ]
			* self	= make ( DEPQ, 0 )

		case	2 :

			item	= ( * self ) [ 0 ]
			* self	= ( * self ) [ 1 : 2 ]

		default	:

			size --
			item	= ( * self ) [ 0 ]

//			If last node has high & low, pop low
			if	size % 2 == 1	{
				self.swap ( size -1, size )
			}

			self.swap ( 0, size )

			( * self )	= ( * self ) [ 0 : size ]

			self.propagate_down ( 0 )
	}
	return	item
}

/*	Shows and Removes top Max item from the Priority Queue

	While inserting, the rightmost node might be More or Less than leftmost tree

	Consider Pop_Max :
		      0 4
		   3 4   2 3
		3 4  3 4    2

		->
		      0 2
		   3 4   2 3
		3 4  3 4

		->
		      0 4
		   3 2   2 3
		3 4  3 4

		-> Here Max node ( 2 ) is less than Min node ( 3 ) need to swap them

		-> Property of the interval tree : children of the node ( 3, 4 ) have values 3 <= x <= 4
		=> Thus substituting ( 3, 4 ) with ( 2, 4 ) doesn't require fixing children

		-> Iterating top to bottom & swapping min max guarantees that parents are swapped ( if needed )

		=> After swap continue iterating on the same side ( Max in node ( 2, 3 ) )
		->
		      0 4
		   2 3   2 3
		3 4  3 4

		->
		      0 4
		   2 4   2 3
		3 3  3 4
*/
func ( self  * DEPQ )	Pop_max ()		( item  Item_Interface )	{

	var size	= self.Len ()

	switch	size	{

		case	0 :	return	nil

		case	1 :

			item	= ( * self ) [ 0 ]
			* self	= make ( DEPQ, 0 )

		case	2 :

			item	= ( * self ) [ 1 ]
			* self	= ( * self ) [ 0 : 1 ]

		default	:

			size --
			item	= ( * self ) [ 1 ]
			self.swap ( 1, size )

			( * self )	= ( * self ) [ 0 : size ]

			self.propagate_down ( 1 )
	}
	return	item
}


/*	Propagates the item to the bottom
		Left child	= ( index / 2 ) * 4  + 2  + index % 2
*/
func ( self  * DEPQ )	propagate_down ( index  uint )	{

	var	(
		left_child_index	= ( index / 2 ) * 4  + 2
		right_child_index	= left_child_index + 2

		size	= self.Len ()
		swap_min_max	= func ()	{

			if	( * self ) [ index +1 ].Is_Less ( ( * self ) [ index ] )	{

				self.swap ( index, index +1 )
			}
		}
	)
//	Propagate Max node
	if	index % 2 == 1	{

		swap_min_max	= func ()	{

			if	( * self ) [ index -1 ].Is_More ( ( * self ) [ index ] )	{

				self.swap ( index, index -1 )
			}
		}

		for	left_child_index < size	{

			swap_min_max ()

			if	left_child_index +1 < size	{
				left_child_index ++

//				Use right child when right < left
				if	right_child_index +1 < size	{
					right_child_index ++

					if	( * self ) [ right_child_index ].Is_More ( ( * self ) [ left_child_index ] )	{

						left_child_index	= right_child_index
					}

				} else if	right_child_index < size	&&
					( * self ) [ right_child_index ].Is_More ( ( * self ) [ left_child_index ] )	{

					left_child_index	= right_child_index
				}
			}

			if	( * self ) [ left_child_index ].Is_More ( ( * self ) [ index ] ) == false	{	return	}

			self.swap ( index, left_child_index )
			index	= left_child_index

			left_child_index	= ( index / 2 ) * 4  + 2
			right_child_index	= left_child_index + 2
		}

		if	index % 2 == 1	&& index < size	{	swap_min_max ()	}

		return
	}

	for	left_child_index < size	{

		swap_min_max ()

//		Use right child when right < left

		if	right_child_index < size	&&
			( * self ) [ right_child_index ].Is_Less ( ( * self ) [ left_child_index ] )	{

			left_child_index	= right_child_index
		}

		if	( * self ) [ left_child_index ].Is_Less ( ( * self ) [ index ] ) == false	{	return	}

		self.swap ( index, left_child_index )
		index	= left_child_index

		left_child_index	= ( index / 2 ) * 4  + 2
		right_child_index	= left_child_index + 2
	}

	if	index +1 < size	{	swap_min_max ()	}
}