// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

package depq

import	(
	"fmt"
//	"bitbucket.org/sjbog/depq"
)


type My_Item	struct {
	Value	int
}

func ( self  My_Item )	Is_Less ( other  Item_Interface )	( bool )	{

	return	self.Value < other.( My_Item ).Value
}

func ( self  My_Item )	Is_More ( other  Item_Interface )	( bool )	{

	return	self.Value > other.( My_Item ).Value
}

func ( self  My_Item )	Copy ()	( Item_Interface )	{

	return	My_Item { self.Value }
}

func pop ( pq  PQ_Interface )	Item_Interface	{	return	pq.Pop ()	}

func is_equal ( array  [] int, pq  PQ_Interface )		bool	{

	switch	pq.( type )	{

		case	* PQ	:

			for	i, size := uint ( 0 ), pq.Len () ; i < size ; i ++	{

				if	array [ i ]	!=
					( * pq. ( * PQ )) [ i ] .( My_Item ) .Value	{

					return	false
				}
			}


		case	* DEPQ	:

			for	i, size := uint ( 0 ), pq.Len () ; i < size ; i ++	{

				if	array [ i ]	!=
					( * pq. ( * DEPQ )) [ i ] .( My_Item ) .Value	{

					return	false
				}
			}
	}
	return	true
}

func ExampleDEPQ_Interface_compatibility ()	{

	var pq	= New_DEPQ (
		My_Item { 4 }, My_Item { -4 }, My_Item { 1 }, My_Item { 3 }, My_Item { 5 }, My_Item { 3 } )

	fmt.Println ( is_equal (
		[] int {
				-4, 5,
			1,3,	3,4,
		}, pq,
	))

	for	pq.Len () > 0	{

		fmt.Println ( pop ( pq ) .( My_Item ).Value )
	}

//	Output: true
//-4
//1
//3
//3
//4
//5

}