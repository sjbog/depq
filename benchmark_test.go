// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

package depq

import	(
	"testing"
	"container/heap"
)

// A PriorityQueue implements heap.Interface and holds Items.
type PQ_native	[] My_Item


func ( self  * PQ_native )	Len ()		int	{
	return	len ( * self )
}

func ( self  * PQ_native )	Less ( i, j  int )		bool	{

	return	( * self ) [ i ].Value < ( * self ) [ j ].Value
}

func ( self  * PQ_native )	Swap ( i, j  int )	{

	( * self ) [ i ], ( * self ) [ j ]	= ( * self ) [ j ], ( * self ) [ i ]
}

func ( self  * PQ_native )	Push ( x  interface {} )	{

	* self	= append ( * self, x.( My_Item ) )
}

func ( self  * PQ_native )	Pop ()		interface {}	{

	if	self.Len () == 0	{
		return	nil
	}
	var last_index	= self.Len () -1
	var item	= ( * self ) [ last_index ]

	( * self )	= ( * self ) [ 0 : last_index ]

	return	item
}


//--------------------------------------------

const	(

	start	= 5
	size	= 5
)

var	(
	pq_depq		= New_PQ ( My_Item { 0 } )
	pq_native	= & PQ_native { My_Item { 0 } }
)

func Benchmark_Push_PQ ( b  * testing.B )	{

	var	(
		pq	= new ( PQ )
		ti	int
		i	= start
	)

	for	ti = 0 ; ti < b.N ; ti ++	{

		* pq	= * pq_depq

		for	pq.Len () < size	{

			pq.Push ( My_Item { i } )
			i	-= 2
			pq.Push ( My_Item { i } )
			i	+= 5
		}
	}
	* pq_depq	= * pq
}

func Benchmark_Push_Native ( b  * testing.B )	{

	var	(
		pq	= new ( PQ_native )
		ti	int
		i	= start
	)

	for	ti = 0 ; ti < b.N ; ti ++	{

		* pq	= * pq_native

		for	pq.Len () < size	{

			heap.Push ( pq, My_Item { i } )
			i	-= 2
			heap.Push ( pq, My_Item { i } )
			i	+= 5
		}
	}
	* pq_native	= * pq
}

func Benchmark_Pop_PQ ( b  * testing.B )	{

	var	(
		ti	int
		pq	= new ( PQ )
		item	My_Item
		prev	My_Item
	)

	for	ti = 0 ; ti < b.N ; ti ++	{

		* pq	= make ( PQ, pq_depq.Len () )
		copy ( * pq, * pq_depq )

		prev	= pq.Pop ().( My_Item )

		for	pq.Len () > 0	{

			item	= pq.Pop ().( My_Item )

			if	item.Value <= prev.Value	{


				b.Errorf ( "Expected a min prority queue, got %v <= %v \t %v, %v", item.Value, prev.Value, pq.Pop ().( My_Item ), pq.Pop ().( My_Item ) )
				b.FailNow ()
			}

			prev	= item
		}
	}
}

func Benchmark_Pop_Native ( b  * testing.B )	{

	var	(
		ti	int
		pq	= new ( PQ_native )
		item	My_Item
		prev	My_Item
	)

	for	ti = 0 ; ti < b.N ; ti ++	{

		* pq	= make ( PQ_native, pq_native.Len () )
		copy ( * pq, * pq_native )

		prev	= heap.Pop ( pq ).( My_Item )

		for	pq.Len () > 0	{

			item	= heap.Pop ( pq ).( My_Item )

			if	item.Value <= prev.Value	{

				b.Errorf ( "Expected a min prority queue, got %v <= %v", item.Value, prev.Value )
				b.FailNow ()
			}

			prev	= item
		}
	}
}

