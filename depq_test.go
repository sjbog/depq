// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

package depq

import	"testing"

func Test_DEPQ_edge_cases ( t  * testing.T )	{

	t.Parallel ()

	var pq	= New_DEPQ (
		My_Item { 15 }, My_Item { -1 }, My_Item { 11 }, My_Item { 2 }, My_Item { 4 }, My_Item { 8 },
	)

	if	pq.Peek ().( My_Item ).Value != -1	{

		t.Errorf ( "Peek min should give -1, got : %v", pq.Peek () )
		t.FailNow ()
	}

	if	pq.Peek ().( My_Item ).Value != -1	{

		t.Errorf ( "Peek min should give -1, got : %v", pq.Peek () )
		t.FailNow ()
	}

	if	pq.Peek_max ().( My_Item ).Value != 15	{

		t.Errorf ( "Peek max should give 15, got : %v", pq.Peek_max () )
		t.FailNow ()
	}

	pq = new ( DEPQ )

	if	pq.Peek () != nil	|| pq.Pop () != nil	{

		t.Error ( "Pop should be nil" )
		t.FailNow ()
	}
	if	pq.Peek_max () != nil	|| pq.Pop_max () != nil	{

		t.Error ( "Pop should be nil" )
		t.FailNow ()
	}

	pq.Merge ( nil )
	pq.Merge ( New_DEPQ () )
	pq.Merge ( New_DEPQ ( My_Item { 11 } ) )

	if	pq.Peek ().( My_Item ).Value != 11	{
		t.Errorf ( "Peek should give 11, got : %v", pq.Pop () )
		t.FailNow ()
	}

	pq.Merge ( New_DEPQ ( My_Item { 15 } ) )

	if	pq.Peek_max ().( My_Item ).Value != 15	{
		t.Errorf ( "Peek should give 15, got : %v", pq.Pop_max () )
		t.FailNow ()
	}
}

func Test_DEPQ_swap_overload ( t  * testing.T )	{

	t.Parallel ()

	var pq	= New_DEPQ (
		My_Item { 0 }, My_Item { 5 },
		My_Item { 0 }, My_Item { 2 }, My_Item { 3 }, My_Item { 4 },

		My_Item { 0 }, My_Item { 1 }, My_Item { 1 }, My_Item { 2 },
			My_Item { 4 }, My_Item { 4 },
	)

	var expected	= [] int {
				0,5,
			0,2,	3,4,

		0,1,1,2,	4,4,
	}

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}

	* pq	= DEPQ {
		My_Item { 0 }, My_Item { 5 },
		My_Item { 0 }, My_Item { 2 }, My_Item { 3 }, My_Item { 4 },

		My_Item { 0 }, My_Item { 1 }, My_Item { 1 }, My_Item { 2 },
			My_Item { 4 }, My_Item { 4 },
	}
	pq.Heapify ( 1 )

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}

	pq.Pop ()

	expected	= [] int {
				0,5,
			0,4,	3,4,

		1,2,1,2,	4,
	}

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}

	pq	= New_DEPQ (
		My_Item { 0 }, My_Item { 5 },
		My_Item { 1 }, My_Item { 4 }, My_Item { 0 }, My_Item { 4 },

		My_Item { 1 }, My_Item { 2 }, My_Item { 2 }, My_Item { 3 },
			My_Item { 0 },
	)

	expected	= [] int {
				0,5,
			1,4,	0,4,

		1,2,2,3,	0,
	}
	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}
	pq.Pop_max ()

	expected	= [] int {
				0,4,
			0,3,	0,4,

		1,2,1,2,
	}
	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}


	pq	= New_DEPQ (
		My_Item { 0 }, My_Item { 5 },
		My_Item { 1 }, My_Item { 3 }, My_Item { 0 }, My_Item { 4 },

		My_Item { 1 }, My_Item { 2 }, My_Item { 2 }, My_Item { 3 },
			My_Item { 1 }, My_Item { 2 }, My_Item { 3 }, My_Item { 4 },
	)
	expected	= [] int {
				0,5,
			1,3,	0,4,

		1,2,2,3,	1,2,3,4,
	}
	pq.Pop ()
	expected	= [] int {
				0,5,
			1,3,	1,4,

		1,2,2,3,	2,3,4,
	}
	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}


	pq	= New_DEPQ (
		My_Item { 0 }, My_Item { 5 },
		My_Item { 1 }, My_Item { 3 }, My_Item { 0 }, My_Item { 4 },

		My_Item { 1 }, My_Item { 2 }, My_Item { 2 }, My_Item { 3 },
			My_Item { 1 }, My_Item { 2 }, My_Item { 4 },
	)
	expected	= [] int {
				0,5,
			1,3,	0,4,

		1,2,2,3,	1,2,4,
	}
	pq.Pop ()
	expected	= [] int {
				0,5,
			1,3,	1,4,

		1,2,2,3,	2,4,
	}
	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}


	pq	= New_DEPQ (
		My_Item { 0 }, My_Item { 5 },
		My_Item { 1 }, My_Item { 3 }, My_Item { 0 }, My_Item { 4 },

		My_Item { 1 }, My_Item { 2 }, My_Item { 2 }, My_Item { 3 },
			My_Item { 1 }, My_Item { 4 }, My_Item { 3 },
	)
	expected	= [] int {
				0,5,
			1,3,	0,4,

		1,2,2,3,	1,4,3,
	}
	pq.Pop_max ()
	expected	= [] int {
				0,4,
			1,3,	0,4,

		1,2,2,3,	1,3,
	}
	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}
}

func Test_DEPQ_duplicates ( t  * testing.T )	{

	t.Parallel ()

	var pq	= new ( DEPQ )
	var v	int

	for	start, size := -1, -1 ; size <= 11 ; size ++	{

		for	i := start ; i < size ; i ++	{

			pq.Push ( My_Item { i } )
			pq.Push ( My_Item { i } )
		}

		for	i := start ; i < size ; i ++	{


			v = pq.Pop ().( My_Item ).Value

			if	v != i	{

				t.Errorf ( "[ Pop ], expected : %v, got %v", i, v )
				t.FailNow ()
			}


			v = pq.Pop ().( My_Item ).Value

			if	v != i	{

				t.Errorf ( "[ Pop ], expected : %v, got %v", i, v )
				t.FailNow ()
			}
		}

		for	i := size -1 ; i >= start ; i --	{

			pq.Push ( My_Item { i } )
			pq.Push ( My_Item { i } )
		}

		for	i := size -1 ; i >= start ; i --	{

			v = pq.Pop_max ().( My_Item ).Value

			if	v != i	{

				t.Errorf ( "[ Pop max ], expected : %v, got %v", i, v )
				t.FailNow ()
			}

			v = pq.Pop_max ().( My_Item ).Value

			if	v != i	{

				t.Errorf ( "[ Pop max ], expected : %v, got %v", i, v )
				t.FailNow ()
			}
		}
	}
}

func Test_DEPQ_custom_step ( t  * testing.T )	{

	t.Parallel ()

	var pq	= new ( DEPQ )
	var prev	int

	for	start, size, step := -1, 0, 3 ; size <= 110 ; size ++	{

		for	i := start ; i < size ; i += step	{

			pq.Push ( My_Item { i } )
		}

		prev	= start -1

		for	pq.Len () > 0	{

			if	pq.Peek ().( My_Item ).Value <= prev	{

				t.Errorf ( "[ Pop ], expected > %v, got %v", prev, pq.Pop ().( My_Item ).Value  )
				t.FailNow ()
			}
			prev	= pq.Pop ().( My_Item ).Value
		}

		for	i := start ; i < size ; i += step	{

			pq.Push ( My_Item { i } )
		}

		prev	= pq.Peek_max ().( My_Item ).Value +1

		for	pq.Len () > 0	{

			if	pq.Peek_max ().( My_Item ).Value >= prev	{

				t.Errorf ( "[ Pop max ], expected < %v, got %v", prev, pq.Pop_max ().( My_Item ).Value  )
				t.FailNow ()
			}
			prev	= pq.Pop_max ().( My_Item ).Value
		}
	}
}