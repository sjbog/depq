// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

package depq

import	(
	"fmt"
//	"bitbucket.org/sjbog/depq"
)

func ExamplePQ_Heapify ()	{

	var	(
		arr	= [] Item_Interface { My_Item { 15 }, My_Item { -1 }, My_Item { 2 }, My_Item { 4 }, My_Item { 8 } }

		pq	= * New_PQ ( arr... )
		pq_raw	= PQ ( arr )
	)

	fmt.Println ( fmt.Sprintf ( "Raw slice : %v", pq_raw ) )
	fmt.Println ( fmt.Sprintf ( "Priority Queue slice : %v", pq ) )

	pq_raw.Heapify ( 1 )	// 0-indexed item will be swapped anyway

	fmt.Println ( fmt.Sprintf ( "After heapify / reorder : %v", pq_raw ) )

	fmt.Println ( fmt.Sprintf ( "Are they equal ? : %v", fmt.Sprintf ( "%v", pq ) == fmt.Sprintf ( "%v", pq_raw ) ) )

//	Output:
//Raw slice : [{15} {-1} {2} {4} {8}]
//Priority Queue slice : [{-1} {4} {2} {15} {8}]
//After heapify / reorder : [{-1} {4} {2} {15} {8}]
//Are they equal ? : true

}

/*	My_Item implements Item_Interface by Value : func ( self  My_Item ) ...
*/
func ExamplePQ_Merge_byValue ()	{

	var	(
		arr	= [] Item_Interface { My_Item { 15 }, My_Item { -1 }, My_Item { 2 }, My_Item { 4 }, My_Item { 8 } }

		pq1	= * New_PQ ( arr... )
		pq2	= PQ ( arr )
	)

	fmt.Println ( fmt.Sprintf ( "Initially Priority Queue 1 : %v", pq1 ) )
	fmt.Println ( fmt.Sprintf ( "Initially Priority Queue 2 : %v", pq2 ) )

//	Shallow copy is OK
	pq1.Merge ( & pq2 )

	fmt.Println ( fmt.Sprintf ( "Shallow merge : %v", pq1 ) )
	fmt.Println ( fmt.Sprintf ( "After Merge Priority Queue 2 : %v", pq2 ) )


//	Output:
//Initially Priority Queue 1 : [{-1} {4} {2} {15} {8}]
//Initially Priority Queue 2 : [{15} {-1} {2} {4} {8}]
//Shallow merge : [{-1} {2} {-1} {4} {8} {15} {2} {15} {4} {8}]
//After Merge Priority Queue 2 : []

}

/*	My_Item implements Item_Interface by Reference : func ( self  * My_Item ) ...
*/
func ExamplePQ_Merge_byReference ()	{

	var	(
		pq1	= * New_PQ ( & My_Item_Ref { -1 }, & My_Item_Ref { 2 }, & My_Item_Ref { 4 }, & My_Item_Ref { 8 } )
		pq2	= PQ { & My_Item_Ref { -1 }, & My_Item_Ref { 2 }, & My_Item_Ref { 4 }, & My_Item_Ref { 8 } }
		pq2_backup	= make ( [] Item_Interface, pq2.Len () )
		tmp	string

		fnCmp	= func ( pq1, pq2  [] Item_Interface )	{
			fmt.Print ( "Do PQ1 & PQ2 reference same item ? : [ " )

			for	_, pq1_value	:= range pq1	{
				tmp	= "no "

				for	_, pq2_value	:= range pq2	{
					if	pq1_value == pq2_value	{
						tmp	= "yes "
						break
					}
				}

				fmt.Print ( tmp )
			}

			fmt.Println ( "]" )
		}
	)
//	fmt.Println ( fmt.Sprintf ( "Initially PQ1 : %v", pq1 ) )
//	fmt.Println ( fmt.Sprintf ( "Initially PQ2 : %v", pq2 ) )

//	Shallow copy is NOT ok
	copy ( pq2_backup, pq2 )
	pq1.Merge ( & pq2 )
	pq2	= pq2_backup

//	fmt.Println ( fmt.Sprintf ( "PQ1 after Shallow merge : %v", pq1 ) )

	fmt.Println ( "Shallow copy merge" )
	fnCmp ( pq1, pq2 )


	pq1	= * New_PQ ( & My_Item_Ref { -1 }, & My_Item_Ref { 2 }, & My_Item_Ref { 4 }, & My_Item_Ref { 8 } )
//	fmt.Println ( fmt.Sprintf ( "New PQ1 : %v", pq1 ) )

	pq1.Merge ( pq2.Copy () )
//	fmt.Println ( fmt.Sprintf ( "PQ1 after Deep copy merge : %v", pq1 ) )

	fmt.Println ( "Deep copy merge" )
	fnCmp ( pq1, pq2 )

//	Initially PQ1 : [0xc082000480 0xc082000488 0xc082000490 0xc082000498]
//	Initially PQ2 : [0xc0820004a0 0xc0820004a8 0xc0820004b0 0xc0820004b8]

//	PQ1 after Shallow merge : [0xc082000480 0xc0820004a0 0xc0820004a8 0xc082000498 0xc082000488 0xc082000490 0xc0820004b0 0xc0820004b8]

//	New PQ1 : [0xc0820004f0 0xc0820004f8 0xc082000500 0xc082000508]
//	PQ1 after Deep copy merge : [0xc0820004f0 0xc082000520 0xc082000528 0xc082000508 0xc0820004f8 0xc082000500 0xc082000530 0xc082000538]

//	Output:
//Shallow copy merge
//Do PQ1 & PQ2 reference same item ? : [ no yes yes no no no yes yes ]
//Deep copy merge
//Do PQ1 & PQ2 reference same item ? : [ no no no no no no no no ]
}

/*	Same as ExamplePQ_Merge_byReference example, but merges PQ and DEPQ
*/
func ExampleDEPQ_Merge_byReferenceCompatibility ()	{

	var	(
		pq1	= * New_PQ ( & My_Item_Ref { -1 }, & My_Item_Ref { 2 }, & My_Item_Ref { 4 }, & My_Item_Ref { 8 } )
		pq2	= * New_DEPQ ( & My_Item_Ref { -1 }, & My_Item_Ref { 2 }, & My_Item_Ref { 4 }, & My_Item_Ref { 8 } )

		pq1_backup	= make ( [] Item_Interface, pq1.Len () )
		pq2_backup	= make ( [] Item_Interface, pq2.Len () )
		tmp	string

		fnCmp	= func ( pq1, pq2  [] Item_Interface )	{
			fmt.Print ( "Do PQ1 & PQ2 reference same item ? : [ " )

			for	_, pq1_value	:= range pq1	{
				tmp	= "no "

				for	_, pq2_value	:= range pq2	{
					if	pq1_value == pq2_value	{
						tmp	= "yes "
						break
					}
				}

				fmt.Print ( tmp )
			}

			fmt.Println ( "]" )
		}
	)
//	fmt.Println ( fmt.Sprintf ( "Initially PQ1 : %v", pq1 ) )
//	fmt.Println ( fmt.Sprintf ( "Initially PQ2 : %v", pq2 ) )

//	Shallow copy is NOT ok

	copy ( pq1_backup, pq1 )
	copy ( pq2_backup, pq2 )
	pq1.Merge ( & pq2 )
	pq2	= pq2_backup

//	fmt.Println ( fmt.Sprintf ( "PQ1 after Shallow merge : %v", pq1 ) )
//	fmt.Println ( fmt.Sprintf ( "PQ2 after Shallow merge : %v", pq2 ) )

	fmt.Println ( "Shallow copy merge" )
	fnCmp ( pq1, pq2 )

	pq1	= pq1_backup
//	fmt.Println ( fmt.Sprintf ( "New PQ1 : %v", pq1 ) )

	pq1.Merge ( pq2.Copy () )
//	fmt.Println ( fmt.Sprintf ( "PQ1 after Deep copy merge : %v", pq1 ) )
//	fmt.Println ( fmt.Sprintf ( "PQ2 after Deep copy merge : %v", pq2 ) )
//
	fmt.Println ( "Deep copy merge" )
	fnCmp ( pq1, pq2 )
	pq1	= pq1_backup


//	Shallow copy is NOT ok
	pq2.Merge ( & pq1 )
	pq1	= pq1_backup

	fmt.Println ( "Shallow copy merge" )
	fnCmp ( pq2, pq1 )

	pq2	= pq2_backup

	pq2.Merge ( pq1.Copy () )
	fmt.Println ( "Deep copy merge" )
	fnCmp ( pq2, pq1 )


//	Output:
//Shallow copy merge
//Do PQ1 & PQ2 reference same item ? : [ no yes yes no no no yes yes ]
//Deep copy merge
//Do PQ1 & PQ2 reference same item ? : [ no no no no no no no no ]
//Shallow copy merge
//Do PQ1 & PQ2 reference same item ? : [ no no no yes yes yes yes no ]
//Deep copy merge
//Do PQ1 & PQ2 reference same item ? : [ no no no no no no no no ]
}