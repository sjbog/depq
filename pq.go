// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

/*
Priority Queue uses struct comparison methods instead of package ones.
Thus gives the ability to build custom Priority Queue, while "container/heap" is array ( slice ) based only.

PQ and DEPQ are array ( slice ) based structures, which implement the corresponding interfaces. One can build tree based PQ with the ability to merge any struct which implements the interface. PQ and DEPQ are not thread safe.

	type PQ struct implements Min priority queue.

Notes: go slices don't shrink back, consider this when using huge amounts of data
*/
package depq


/*	Abstract unit ( parent class in OOP ), which PQ and DEPQ produce and consume
*/
type Item_Interface	interface {

	Is_Less ( Item_Interface )	bool
	Is_More ( Item_Interface )	bool

	// to make full ( deep ) copy, not by reference ( shallow )
	Copy ()	Item_Interface
}

/*	Array ( slice ) based implementation of Min priority queue.	Implements PQ_Interface.
*/
type PQ	[] Item_Interface

/*	Abstract unit ( abstract class in OOP ), which defines the behaviour ( methods )
*/
type PQ_Interface	interface {

	Len		()		uint
	Push	(Item_Interface)
	Pop		()		Item_Interface
	Peek	()		Item_Interface

//	In-place restructure
	Heapify	( offset  uint )

//	Copy by reference on pointer values
	Merge	( other  PQ_Interface )

//	Makes deep copy, calls Item_Interface.Copy ()
	Copy	()	PQ_Interface
}


/*	Creates and fills a new Priority Queue
*/
func New_PQ	( items  ... Item_Interface )	( * PQ )	{
	var	(
		size	= len ( items )
//		Pre-allocate size
		pq		= make ( PQ, 0, size +8 )
	)

	for	i := 0 ; i < size ; i ++	{
			pq.Push ( items [ i ] )
	}

	return	& pq
}

/*	In-place restructure. Is used to sort raw slice into a Priority Queue ordered slice
*/
func ( self  * PQ )	Heapify ( start_offset  uint )	{

	for	i, size := start_offset, self.Len () ; i < size ; i ++	{
			self.propagate_up ( i )
	}
}

/*	Makes a deep copy of current values. Calls Item_Interface.Copy () on every item
*/
func ( self  * PQ )	Copy ()	( PQ_Interface )	{
	var	(
		size	= self.Len ()
		pq	= make ( PQ, size, cap ( * self ) )
	)

	for	i := uint ( 0 ); i < size ; i ++	{
		pq [ i ]	= ( * self )[ i ].Copy ()
	}

	return	& pq
}

/*	Pops values of `other` PQ into self. Copy by reference on pointer values ( shallow copy )

		Warning : this operation will purge `other` ( it will be empty ).

	To make a merge with deep copy use
		pq1.Merge ( pq2.Copy () )
*/
func ( self  * PQ )	Merge ( other  PQ_Interface )	{

	if	other == nil	{	return	}

	var size	= other.Len ()

	if	size == 0	{	return	}

//	Fast copy for 2 slice based PQs
	if	other_pq, ok	:= other.( * PQ ); ok	&& self.Len () == 0	{

		* self	= * other_pq
		self.Heapify ( 1 )
		return

	}	else	{
//	Shallow copy by PQ_Interface
		for	i := uint ( 0 ) ; i < size ; i++	{
			self.Push ( other.Pop () )
		}
	}
}

/*	Returns number of items in the underlying array ( length of slice, not capacity )
*/
func ( self  * PQ )	Len ()		uint	{	return	uint ( len ( * self ) )	}

/*	Appends item to the slice and propagates it up
*/
func ( self  * PQ )	Push ( item  Item_Interface )	{

	( * self )	= append ( ( * self ), item )
	self.propagate_up ( self.Len () -1 )
}

/*	Propagates the item to the top
*/
func ( self  * PQ )	propagate_up ( index  uint )	{

	var parent_index	uint	= ( index -1 ) / 2

	for	index > 0	&&
		( * self ) [ index ].Is_Less ( ( * self ) [ parent_index ] )	{

		self.swap ( index, parent_index )
		index	= parent_index
		parent_index	= ( index -1 ) / 2
	}
}

/*	Propagates the item to the bottom
*/
func ( self  * PQ )	propagate_down ( index  uint )	{

	var	(
		left_child_index	= index * 2 + 1
		right_child_index	= left_child_index + 1
		size	= self.Len ()
	)

	for	right_child_index < size	{

//		Use right child when right < left

		if	( * self ) [ right_child_index ].Is_Less ( ( * self ) [ left_child_index ] )	{

			left_child_index	= right_child_index
		}

		if	( * self ) [ left_child_index ].Is_Less ( ( * self ) [ index ] ) == false	{
			return
		}

		self.swap ( index, left_child_index )
		index	= left_child_index

		left_child_index	= index * 2 + 1
		right_child_index	= left_child_index + 1
	}

	if	left_child_index < size	&&
		( * self ) [ left_child_index ].Is_Less ( ( * self ) [ index ] )	{

		self.swap ( index, left_child_index )
	}
}

/*	Swaps slice items by indices
*/
func ( self  * PQ )	swap ( index1, index2  uint )	{

	( * self )[ index1 ], ( * self )[ index2 ]	= ( * self )[ index2 ], ( * self )[ index1 ]
}

/*	Shows top Min item but doesn't pop it
*/
func ( self  * PQ )	Peek ()		Item_Interface	{
	if	self.Len () == 0	{
		return	nil
	}
	return	( * self ) [ 0 ]
}

/*	Shows and Removes top Min item from the Priority Queue
*/
func ( self  * PQ )	Pop ()		( item  Item_Interface )	{

	var size	= self.Len ()

	switch	size	{

		case	0 :	return	nil

		case	1 :
			item	= ( * self ) [ 0 ]
			* self	= make ( PQ, 0 )

		default	:

			size --
			item	= ( * self ) [ 0 ]
			self.swap ( 0, size )

			( * self )	= ( * self ) [ 0 : size ]

			self.propagate_down ( 0 )
	}
	return	item
}