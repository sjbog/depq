// Copyright (c) 2013, Bogdan S.
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file

package depq

import	"testing"

func Test_PQ ( t  * testing.T )	{

	t.Parallel ()

	var pq	= New_PQ ( My_Item { 15 }, My_Item { -1 }, My_Item { 2 }, My_Item { 4 }, My_Item { 8 } )

	var expected	= [] int {
			 -1,
			4,	2,
		 15, 8,
	}

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}

	* pq	= PQ { My_Item { 15 }, My_Item { -1 }, My_Item { 2 }, My_Item { 4 }, My_Item { 8 } }
	pq.Heapify ( 1 )

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}

	pq.Pop ()

	expected	= [] int {
			 2,
			4,	8,
		 15,
	}

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}

	pq.Push ( My_Item { 8 } )
	pq.Push ( My_Item { 8 } )
	pq.Pop ()

	expected	= [] int {
			 4,
			8,	8,
		 15, 8,
	}

	if	! is_equal ( expected, pq )	{

		t.Errorf ( "Didn't get the expected : %v, result : %v", expected, * pq )
		t.FailNow ()
	}


	pq.Merge ( nil )
	pq.Merge ( new ( PQ ) )

	pq.Merge (
		New_PQ ( My_Item { 0 }, My_Item { 5 }, My_Item { 6 }, My_Item { 7 }, My_Item { 8 } ),
	)

	if	pq.Peek ().( My_Item ).Value != 0	{
		t.Errorf ( "Peek should give 0, got : %v", pq.Pop () )
		t.FailNow ()
	}

	pq	= new ( PQ )

	if	pq.Peek () != nil	|| pq.Pop () != nil	{

		t.Error ( "Pop should be nil" )
		t.FailNow ()
	}

	pq.Merge (
		New_PQ ( My_Item { 0 }, My_Item { 2 }, My_Item { 1 }, My_Item { 3 }, My_Item { 4 }, My_Item { 5 }, My_Item { 6 } ),
	)

	for	i := 0 ; i < 7 ; i ++	{

		if	pq.Peek ().( My_Item ).Value != i	{
			t.Errorf ( "Pop should give %v, got : %v", i, pq.Pop () )
			t.FailNow ()
		}
		pq.Pop ()
	}
}